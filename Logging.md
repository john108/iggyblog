# Logrotation on EC2 Instances

It is all too common that you run a process on an ec2 instance, and log the output to standardout. These logs have three issues:

* Accessibility - have to ssh onto the server
* Persistence - if the job stops, the logs are gone. Debugging becomes a pain.
* Structuring - It is difficult to easily jump to a specific day or type.

Here, we will use `AWS Cloudwatch`, EC2, screen, and logrotate to setup a system for storing the logs into log files, rotating those (adding daily extensions and deleting old logs), and then shipping these to AWS Cloudwatch Log Groups.

This is a normal system for logging. You typically want to do some type of log-rotation - whether that is because of size constraints, or chronological constraints.

## Some other recommended readings/tutorials.

## Save standardout (from screen) to a log file

First, you need to get the output from a screen terminal saving to a log file. This can be done with:

```
/usr/bin/screen -L -Logfile /home/ubuntu/log/fpg/fpgtrader.log -dmS fpg_trader /bin/bash /home/ubuntu/fpg-trader/start_trader_screen.sh
```

where `/home/ubuntu/log/fpg/fpgtrader.log` is the file that it writes to.

A more detailed writeup can be found at:

https://unix.stackexchange.com/questions/305950/how-can-i-save-the-output-of-a-detached-screen-with-script

## Rotate log files by days, delete old records
 We use: https://httpd.apache.org/docs/2.4/programs/rotatelogs.html or https://linux.die.net/man/8/rotatelogs.

This is a program that will run at a set interval (typically once a day) and handle the rotation/division of a single log file into many others.

See this: https://serverfault.com/a/828994, for an example template file, or: https://www.tecmint.com/install-logrotate-to-manage-log-rotation-in-linux/.

Our log ended up as:
```
# /etc/logrotate.d/fpg  # Name of the file created.
"/home/ubuntu/log/fpg/fpgtrader.log" {  # This should be the name of the logfile
        daily  # Run once a day
        rotate 5  # Only keep the last 5 records
        su root root  # Solves sudo problems when accessing non-root directories
	dateext  # Add a date extension for the file
        postrotate
                echo "A rotation just took place."  # Log after it completes
        endscript
}
```

When testing, it is useful to use:
```
# /etc/logrotate.d/fpg
"/home/ubuntu/log/fpg/fpgtrader.log" {
        daily
        rotate 5
        su root root
        postrotate
                echo "A rotation just took place."
        endscript
        size=1k
	dateext
}
```
Which will rotate files that > than 1k - this is an easy way to make sure small log files are broken up.

After you write this file in `/etc/logrotate.d/<filename>`, the logrotate system will automatically pick it up. You can manually call it with: `sudo logrotate /etc/logrotate.d/fpg`.

This shows a rotation:
```
ubuntu@ip-172-31-41-237:~/log/fpg$ ls -lth
total 88K
-rw-rw-r-- 1 ubuntu ubuntu 2.1K May  9 17:01 fpgtrader.log
-rw-rw-r-- 1 ubuntu ubuntu  14K May  9 17:01 fpgtrader.log.1
-rw-rw-r-- 1 ubuntu ubuntu 2.0K May  9 16:59 fpgtrader.log.2
-rw-rw-r-- 1 ubuntu ubuntu  13K May  9 16:59 fpgtrader.log.3
-rw-rw-r-- 1 ubuntu ubuntu 2.1K May  9 16:57 fpgtrader.log.4
-rw-rw-r-- 1 ubuntu ubuntu  38K May  9 16:56 fpgtrader.log.5
ubuntu@ip-172-31-41-237:~/log/fpg$ ls -lth
total 44K
-rw-rw-r-- 1 ubuntu ubuntu 2.1K May  9 17:01 fpgtrader.log.1
-rw-rw-r-- 1 ubuntu ubuntu  14K May  9 17:01 fpgtrader.log.2
-rw-rw-r-- 1 ubuntu ubuntu 2.0K May  9 16:59 fpgtrader.log.3
-rw-rw-r-- 1 ubuntu ubuntu  13K May  9 16:59 fpgtrader.log.4
-rw-rw-r-- 1 ubuntu ubuntu 2.1K May  9 16:57 fpgtrader.log.5
```

## Send log files to cloudwatch

Install cloudwatch:

1. wget https://s3.amazonaws.com/amazoncloudwatch-agent/ubuntu/amd64/latest/amazon-cloudwatch-agent.deb
2. sudo dpkg -i -E ./amazon-cloudwatch-agent.deb

Once you get it installed, you should run the configuration wizard (to tell it what file to start watching) with:

```
sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-config-wizard
```

Note this will create a config file at:

`/opt/aws/amazon-cloudwatch-agent/bin/config.json`

You will need to copy + paste the contents of this over to:

`/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json`

A sample config is:
```
{
        "agent": {
                "run_as_user": "root"
        },
        "logs": {
                "logs_collected": {
                        "files": {
                                "collect_list": [
                                        {
                                                "file_path": "/home/ubuntu/log/fpg/**",
                                                "log_group_name": "fpg",
                                                "log_stream_name": "{instance_id}"
                                        }
                                ]
                        }
                }
        }
}
```

Note if you have any issue, the cloudwatch client sends logs to (more details can be found in: https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/CloudWatch-Agent-Configuration-File-Details.html):

`vim /opt/aws/amazon-cloudwatch-agent/logs/amazon-cloudwatch-agent.log`

and it can be restarted with:

`sudo systemctl restart amazon-cloudwatch-agent`

After all this you should see the logs in your cloudwatch group!




